# Mastermind


## About

This repo is about different algorithms for playing the game Mastermind and benchmarking their performance.
The code is written in pure Python 3.

## Description
The traditional Mastermind game where the correct color (out of 6) needs to be determined for each of the 4 positions is played. 

The currently best AI, which is based on information entropy, requires an average of 4.42 guesses to complete the game.

For each simulated game, the searched combination, the made guesses and all provided feedback is logged.
Depending on the specific AI more detailed information may also be available (e.g. entropy).

## Results

The following numbers were computed by running each AI over all possible 1296 games.

"Excluded Candidates" are those combinations which cannot possibly be the one to be determined based on the current game state.

AI | Average Number of Needed Guesses
--- |---
AIExclusionHighestEntropy - ConsideringExcludedCandidates | `4.42`
AIExclusionHighestEntropy - NotConsideringExcludedCandidates | 4.47
AIExclusionHighestMinimalGain - ConsideringExcludedCandidates | 4.48
AIExclusionHighestMinimalGain - NotConsideringExcludedCandidates | 4.49
AIExclusionRandom | 4.60

### Detailed Statistics

Considering Excluded Candidates | Not Considering Excluded Candidates
--- |---
![](results/AIExclusionHighestEntropy_ConsideringExcludedCandidates.png) | ![](results/AIExclusionHighestEntropy_NotConsideringExcludedCandidates.png)
![](results/AIExclusionHighestMinimalGain_ConsideringExcludedCandidates.png) | ![](results/AIExclusionHighestMinimalGain_NotConsideringExcludedCandidates.png)
Does not make sense. | ![](results/AIExclusionRandom.png)


### Played Games
If you want to see some example games the AIs have played have a look within the results folder.
It contains the entire game history (all guesses, provided feedback and additional data) of every possible game for all AIs.

## Installation
This project requires a Python interpreter (version 3.8+) and two additional dependencies (matplotlib and tqdm).

## Usage
Just have a look at main.py where you can configure the AIs and some additional settings for benchmarking them.
By default, each AI plays all possible 6^4=1296 games once.

## Authors and acknowledgment
Author: Sebastian Haushofer

Thanks to Grant Sanderson (a.k.a. 3Blue1Brown) for inspiring me to do this small project.

## License
This is free and open source, use and extend this however you may like ;).
