from src.mastermind.ai.ai_exclusion_highest_entropy import AIExclusionHighestEntropy
from src.mastermind.ai.ai_exclusion_highest_minimal_gain import AIExclusionHighestMinimalGain
from src.mastermind.ai.ai_exclusion_random import AIExclusionRandom
from src.mastermind.ai.ai_simulator import AISimulator
from src.mastermind.color import Color
from src.mastermind.combination import Combination


def main():
    # Fix the initial guesses so that the results are reproducible (except for the random AI).
    first_guess_for_entropy = Combination((Color.GREEN, Color.BLUE, Color.PURPLE, Color.YELLOW))
    first_guess_for_minimal_gain = Combination((Color.GREEN, Color.PURPLE, Color.PURPLE, Color.GREEN))
    # Set AIs to be used for simulation.
    ais = [AIExclusionRandom(),
           AIExclusionHighestEntropy(consider_excluded_candidates=True, first_guess=first_guess_for_entropy),
           AIExclusionHighestEntropy(consider_excluded_candidates=False, first_guess=first_guess_for_entropy),
           AIExclusionHighestMinimalGain(consider_excluded_candidates=True, first_guess=first_guess_for_minimal_gain),
           AIExclusionHighestMinimalGain(consider_excluded_candidates=False, first_guess=first_guess_for_minimal_gain)]
    # Optionally, the number of games to be run for the benchmark can be limited.
    # By default, all 6^4=1296 possible target combinations will be simulated for each AI.
    # The more games are run, the more accurate the benchmark becomes.
    limit_n_games = None
    # For each AI, the history of every game it has played will be logged to a file.
    # Optionally, this can also be logged to the console while running.
    log_games_to_console = False
    ai_simulator = AISimulator(ais)
    ai_simulator.benchmark(log_games_to_console=log_games_to_console, limit_n_games=limit_n_games)


if __name__ == '__main__':
    main()
