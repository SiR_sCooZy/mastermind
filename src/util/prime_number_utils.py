from enum import IntEnum
from typing import Callable


def compute_prime_factors(n: int) -> [int]:
    i = 2
    prime_factors = []
    while i * i <= n:
        if n % i != 0:
            i += 1
        else:
            n = n // i
            prime_factors.append(i)
    if n > 1:
        prime_factors.append(n)
    return prime_factors


def decode_using_prime_factorization(encoded_number: int, decoder: Callable[[int], IntEnum]) -> [IntEnum]:
    factors = compute_prime_factors(encoded_number)
    return [decoder(f) for f in factors]
