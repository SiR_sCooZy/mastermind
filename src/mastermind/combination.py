from __future__ import annotations

import pickle
import random
from collections import Counter
from math import prod
from pathlib import Path

from tqdm import tqdm

from src.mastermind.color import Color
from src.mastermind.hint import Hint
from src.mastermind.result import Result


class Combination:
    """
    This can represent both a specific guess and the true pattern which needs to be figured out.
    One combination consists of four colors where the order of placement matters.
    """

    _ALL_COMBINATION_RESULTS_CACHE = None

    def __init__(self, colors: (Color, Color, Color, Color)):
        self._colors = colors
        bases = [2, 3, 5, 7]
        exponents = colors
        # use prime numbers for positional encoding
        # this guarantees unique ids and reconstruction from a given id
        self._id = prod([b ** e for b, e in zip(bases, exponents)])
        # store all color counts for faster result computation
        self._colors_dict = {c: 0 for c in Color}
        self._colors_dict.update(dict(Counter(self._colors)))

    @property
    def id(self) -> int:
        return self._id

    @classmethod
    def get_random(cls) -> Combination:
        """
        :return: A random color combination where each combination is equally likely.
        """
        return cls(tuple([list(Color)[random.randint(0, len(Color) - 1)] for _ in range(4)]))

    @classmethod
    def get_random_all_colors_different(cls) -> Combination:
        """
        :return: A random combination where no color occurs twice.
                 From the combinations which meet this criterion all are equally likely.
        """
        return cls(tuple(random.sample(list(Color), 4)))

    @classmethod
    def get_random_two_different_colors(cls) -> Combination:
        """
        :return: A random combination consisting only of two colors where both colors occur twice.
                 The order of the 4 pins and the chosen colors are random.
                 From the combinations which meet this criterion all are equally likely.
        """
        colors = random.sample(list(Color), 2) * 2
        random.shuffle(colors)
        return cls(tuple(colors))

    @staticmethod
    def get_all_combinations() -> [Combination]:
        """
        :return: List of all possible combinations in the game.
        """
        colors = [col for col in Color]
        combinations = []
        for i in colors:
            for j in colors:
                for k in colors:
                    for m in colors:
                        combinations.append(Combination((i, j, k, m)))
        return combinations

    @staticmethod
    def _compute_all_combination_results() -> {int: {int: Result}}:
        """
        Compute a matrix which stores which result is given for any pair of two combinations.
        This can be used as a cache to simulate many games more efficiently.
        :return: Result matrix represented by a nested dict.
        """
        all_combos = Combination.get_all_combinations()
        cache = {c.id: {} for c in all_combos}
        print("Setting up cache for quicker result computations. This may take a bit.")
        for i in tqdm(range(len(all_combos)), desc="Computing results cache for all combinations"):
            for j in range(i, len(all_combos)):
                c1 = all_combos[i]
                c2 = all_combos[j]
                res = c1.compute_similarity(c2)
                cache[c1.id][c2.id] = res
                cache[c2.id][c1.id] = res
        return cache

    @staticmethod
    def get_all_combination_results(use_file_system_cache: bool = True) -> {int: {int, Result}}:
        """
        This will cache the Results for all possible two pairs of combinations into main memory.
        Optionally, this data can then be written to the file system so that the next time the program is run,
        the setup will be quicker by reading the data from the file system instead of recomputing it.
        :param use_file_system_cache: If True, enable dumping/ loading the computed values from disk.
                                      If False, newly computed each time this program is run.
        :return: Result matrix represented by a nested dict.
        """
        if Combination._ALL_COMBINATION_RESULTS_CACHE is None:
            if use_file_system_cache:
                cached_file_path = Path(__file__).parent / "cache" / "all_combination_results.pickle"
                if Path.exists(cached_file_path.parent) and Path.exists(cached_file_path):
                    print("Loading cached data. This might take briefly.")
                    with open(cached_file_path, 'rb') as f:
                        Combination._ALL_COMBINATION_RESULTS_CACHE = pickle.load(f)
                else:
                    Combination._ALL_COMBINATION_RESULTS_CACHE = Combination._compute_all_combination_results()
                    Path(cached_file_path.parent).mkdir(exist_ok=True, parents=True)
                    with open(cached_file_path, 'wb') as f:
                        pickle.dump(Combination._ALL_COMBINATION_RESULTS_CACHE, f)
            else:
                Combination._ALL_COMBINATION_RESULTS_CACHE = Combination._compute_all_combination_results()
        return Combination._ALL_COMBINATION_RESULTS_CACHE

    def compute_similarity(self, combo: Combination) -> Result:
        """
        Compute the similarity between two combinations.
        This can be used to check how close a guess is to the true pattern.
        This method is commutative, meaning a.compute_similarity(b) is the same as b.compute_similarity(a).
        :param combo: For which similarity shall be computed.
        :return: Similarity between the combinations.
        """
        n_black = sum([g == t for g, t in zip(self._colors, combo._colors)])
        n_white = sum([min(g, t) for g, t in [(self._colors_dict[col], combo._colors_dict[col]) for col in Color]])
        n_white -= n_black
        n_empty = (4 - n_black - n_white)
        return Result(tuple([Hint.BLACK] * n_black + [Hint.WHITE] * n_white + [Hint.EMPTY] * n_empty))

    def __str__(self) -> str:
        return "\t".join([str(col).split(".")[1] for col in list(self._colors)])

    def __eq__(self, other: Combination) -> bool:
        if other is None:
            return False
        return self.id == other.id

    def __hash__(self) -> int:
        return hash(self.id)

    def __lt__(self, other: Combination) -> bool:
        return self.id < other.id
