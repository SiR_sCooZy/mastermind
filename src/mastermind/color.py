from enum import IntEnum


class Color(IntEnum):
    """
    The colors of the pins the player can choose from in the game when making a guess
    """
    ORANGE = 2
    BLUE = 3
    YELLOW = 4
    GREEN = 5
    PINK = 6
    PURPLE = 7
    # Some versions of mastermind also allow leaving gaps in between individual pins.
    # This behaviour can be activated in this codebase by simply uncommenting the following line.
    # EMPTY = 8
