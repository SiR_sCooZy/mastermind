import random

from src.mastermind.ai.ai_exclusion_base import AIExclusionBase
from src.mastermind.combination import Combination


class AIExclusionRandom(AIExclusionBase):
    """
    This AI randomly chooses one of the combinations which theoretically
    can still be the right answer based on the current game state.
    """

    def __init__(self):
        super().__init__()

    def compute_next_guess(self) -> Combination:
        # All possible candidates are equally likely to be selected.
        return self._candidate_combinations[random.randint(0, self.n_remaining_candidates - 1)]
