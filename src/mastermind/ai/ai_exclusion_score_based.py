from collections import Counter
from typing import Callable, List, Union

from src.mastermind.ai.ai_exclusion_base import AIExclusionBase
from src.mastermind.combination import Combination


class AIExclusionScoreBased(AIExclusionBase):
    """
    This AI computes the probability of receiving each possible Result for each remaining Combination.
    Optionally, it can also compute these probabilities for Combinations which have already been excluded.
    Combinations which are still possible candidates are preferred over excluded Combinations at equal scores.
    The way a score is computed by these probabilities is left open and must be passed to the constructor.
    Additionally, the method by which the initial guess is computed must be provided.
    """

    def __init__(self, score_name: str,
                 score_function: Callable[[List[float]], float],
                 consider_excluded_candidates: bool,
                 first_guess: Union[Combination, Callable[[], Combination]]):
        """
        :param score_name: Name of the score which is being computed by score_function().
        :param score_function: Computes a scalar score out of a vector of probabilities for different results.
        :param consider_excluded_candidates: if True, a combination which logically can not be the right answer
                                             according to the current game state is still chosen as next guess if its
                                             score is higher than the score of any possible candidate combination.
                                             if False, only candidate combinations are chosen at all times.
        :param first_guess: Either a fixed combination to use as first guess or a function which produces
                            a combination as the first guess without using any inputs.
        """
        super().__init__()
        self._prev_score = 0
        self._prev_guess_is_candidate = True
        self._consider_excluded_candidates = consider_excluded_candidates
        self._score_function = score_function
        self._score_name = score_name
        self._first_guess = first_guess

    def compute_next_guess(self) -> Combination:
        if self._game.n_guesses == 0:
            # This speeds up computation significantly, as the first guess takes the longest to compute,
            # as it still has most theoretically possible answers and next guess computation is in O(n^2).
            chosen_guess = self._first_guess if type(self._first_guess) == Combination else self._first_guess()
            self._prev_score = self._score_guess(chosen_guess)
            self._prev_guess_is_candidate = True
        else:
            scores = {}
            # All remaining combinations need to be considered
            for candidate_combo in self._candidate_combinations:
                scores[candidate_combo] = (self._score_guess(candidate_combo), True)
            if self._consider_excluded_candidates:
                for excluded_combo in self._excluded_combinations:
                    scores[excluded_combo] = (self._score_guess(excluded_combo), False)
            # Theoretically a simple max would do the job, but the sorting is in O(n * log n)
            # which does not bottleneck compared to the O(n^2) required for the entropy computation.
            scores = dict(sorted(scores.items(), key=lambda item: item[1], reverse=True))
            chosen_guess, (self._prev_score, self._prev_guess_is_candidate) = list(scores.items())[0]
        return chosen_guess

    def get_name(self) -> str:
        if self._consider_excluded_candidates:
            return f"{type(self).__name__}_ConsideringExcludedCandidates"
        else:
            return f"{type(self).__name__}_NotConsideringExcludedCandidates"

    def _score_guess(self, guess: Combination) -> float:
        # Use cache for result computation.
        # This just counts how often each possible Result outcome can occur based on the remaining candidates.
        result_stats = Counter([Combination.get_all_combination_results()[c.id][guess.id].id
                                for c in self._candidate_combinations])
        probabilities = [count / len(self._candidate_combinations) for count in result_stats.values()]
        return self._score_function(probabilities)

    def _get_status_of_last_guess_specific(self) -> str:
        return f"\t-\tIs Candidate: {self._prev_guess_is_candidate}" \
               f"\t-\t{self._score_name}: {self._prev_score:.2f}" \
               f"{super()._get_status_of_last_guess_specific()}"
