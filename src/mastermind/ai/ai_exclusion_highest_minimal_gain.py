from math import log2
from typing import Optional

from src.mastermind.ai.ai_exclusion_score_based import AIExclusionScoreBased
from src.mastermind.combination import Combination


class AIExclusionHighestMinimalGain(AIExclusionScoreBased):
    """
    This AI computes the minimal information gain for each guess based on the current game state and chooses
    the combination with the highest minimal information gain as the next guess.
    If consider_excluded_candidates is set to True, this AI corresponds to Donald Knuth's algorithm :).
    """

    def __init__(self, consider_excluded_candidates: bool, first_guess: Optional[Combination] = None):
        super().__init__(
            score_name="Minimal Information Gain",
            score_function=lambda probabilities: max(0.0, -log2(max([p for p in probabilities]))),
            consider_excluded_candidates=consider_excluded_candidates,
            first_guess=first_guess if first_guess is not None else Combination.get_random_two_different_colors)
