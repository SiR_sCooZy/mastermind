from collections import Counter
from pathlib import Path
from typing import Optional

import matplotlib.pyplot as plt
from tqdm import tqdm

from src.mastermind.ai.ai_base import AIBase
from src.mastermind.combination import Combination
from src.mastermind.game import Game


class AISimulator:
    """
    This class simulates many Mastermind games using the provided AIs.
    """

    def __init__(self, ais: [AIBase]):
        self._ais = ais
        self._results_folder = Path(__file__).parent / ".." / "results"
        self._results_folder.mkdir(exist_ok=True, parents=True)

    def benchmark(self, log_games_to_console: bool = True, limit_n_games: Optional[int] = None) -> None:
        """
        :param log_games_to_console: If True, the entire progress of each game is logged (guesses, results, etc.).
        :param limit_n_games: Number of games to be simulated. If None, all possible 1296 games are played.
        """
        combinations = Combination.get_all_combinations()
        n_games = min(len(combinations), limit_n_games) if limit_n_games is not None else len(combinations)
        simulation_results = {}
        for i, ai in enumerate(self._ais, 1):
            print(f"\n\nSimulating Games with AI ({i}/{len(self._ais)}): {ai.get_name()}")
            ai.log_game_to_console = log_games_to_console
            ai_games_log_path = self._results_folder / f"played_games_{ai.get_name()}.txt"
            if Path.exists(ai_games_log_path):
                # delete old result file
                Path.unlink(ai_games_log_path)
            all_n_guesses = []

            def _run_simulation_loop(combination: Combination):
                ai.new_game(Game(combination))
                all_n_guesses.append(ai.simulate_full_game(ai_games_log_path))

            if log_games_to_console:
                for j in range(n_games):
                    print(f"\n\nSimulating Game {j + 1}")
                    _run_simulation_loop(combinations[j])
            else:
                for j in tqdm(range(n_games), desc="Simulating Games"):
                    _run_simulation_loop(combinations[j])
            # compute number of times n guesses were needed
            guess_counts = {g: 0 for g in list(range(1, max(all_n_guesses) + 1))}
            guess_counts.update(dict(Counter(all_n_guesses)))
            simulation_results[ai.get_name()] = guess_counts
        self._save_benchmark_results(simulation_results)
        self._plot_required_n_guesses(simulation_results)

    def _save_benchmark_results(self, simulation_results: {str: {int: int}}) -> None:
        res_out_path = self._results_folder / f"ai_benchmark_results.txt"
        with open(res_out_path, "w") as f:
            for ai_name, guess_counts in simulation_results.items():
                avg_n_guesses = sum([g * count for g, count in guess_counts.items()]) / sum(guess_counts.values())
                content = [f"Results for {ai_name}",
                           f"Average Number of Needed Guesses: {avg_n_guesses:.2f}"]
                for n_guesses, count in guess_counts.items():
                    if count != 0:
                        content.append(f"Needed {n_guesses} Guesses: {count} Time{'s' if count > 1 else ''}")
                f.write("\n".join(content) + "\n\n")
                [print(line) for line in content]
                print("\n")

    def _plot_required_n_guesses(self, simulation_results: {str: {int: int}},
                                 shared_x_y_range: bool = True) -> None:
        # make plots better comparable by setting same scale on x-axis and y-axis
        x_max = max([max(gcs.keys()) for gcs in simulation_results.values()])
        y_max = max([max(gcs.values()) for gcs in simulation_results.values()])
        for ai_name, guess_counts in simulation_results.items():
            fig, ax = plt.subplots()
            ax.bar(guess_counts.keys(), guess_counts.values())
            ax.set_xlabel("Needed Guesses to Win")
            ax.set_ylabel("Number of Simulations")
            if shared_x_y_range:
                ax.set_xlim([1, x_max + 1])
                ax.set_ylim([0, y_max + max(1, int(y_max / 10))])
            ax.set_title(ai_name)
            out_path = self._results_folder / f"{ai_name}.png"
            out_path.parent.mkdir(exist_ok=True, parents=True)
            fig.savefig(out_path, dpi=300)
