from abc import ABC, abstractmethod
from pathlib import Path
from typing import Optional

from src.mastermind.combination import Combination
from src.mastermind.game import Game
from src.mastermind.result import Result


class AIBase(ABC):
    """
    The base interface for an AI agent to play Mastermind.
    """

    def __init__(self):
        self._game = None
        self._log_game_to_console = True
        self._log_file_handle = None

    @property
    def log_game_to_console(self) -> bool:
        return self._log_game_to_console

    @log_game_to_console.setter
    def log_game_to_console(self, activate_log: bool):
        self._log_game_to_console = activate_log

    def new_game(self, game: Game) -> None:
        """
        Tell the agent that a new game is starting.
        :param game: Defines the searched combination to be determined.
        """
        self._game = game

    def simulate_full_game(self, log_file_path: Optional[Path]) -> int:
        """
        Simulate the entire game and log all guesses as well as the feedback given on them.
        :return: Number of guesses the AI needed to complete the game.
        """
        header_log = f"True Combo: ({str(self._game.true_combination)})"
        if log_file_path is not None:
            self._log_file_handle = open(log_file_path, "a")
            self._log_file_handle.write(header_log + "\n")
        if self._log_game_to_console:
            print(header_log)
        while not self._game.is_won():
            guess = self.compute_next_guess()
            self.simulate_next_guess(guess)
        if log_file_path is not None:
            self._log_file_handle.write("\n")
            self._log_file_handle.close()
            self._log_file_handle = None
        return self._game.n_guesses

    def simulate_next_guess(self, guess: Combination) -> Optional[Result]:
        """
        Simulate a single guess only.
        :param guess: To be made next.
        :return: Feedback (result) for this guess.
        """
        if not self._game.is_won():
            result = self._simulate_next_guess_specific(guess)
            status_message = self._get_status_of_last_guess()
            if self._log_game_to_console:
                print(status_message)
            if self._log_file_handle is not None:
                self._log_file_handle.write(status_message + "\n")
            return result
        return None

    def get_name(self) -> str:
        """
        :return: the name of this AI
        """
        return f"{type(self).__name__}"

    def _get_status_of_last_guess(self) -> str:
        """
        Log statistics regarding the previous guess and feedback.
        """
        base_log = f"Guess {str(self._game.n_guesses)}:\t({str(self._game.last_guess)})\t-\t" \
                   f"Result: ({str(self._game.last_result)})"
        specific_log = self._get_status_of_last_guess_specific()
        return base_log + specific_log

    @abstractmethod
    def compute_next_guess(self) -> Combination:
        """
        The AI can consider the history of the entire game so far to make its next guess.
        :return: The guess the AI has decided to make next.
        """
        ...

    @abstractmethod
    def _simulate_next_guess_specific(self, guess: Combination) -> Result:
        """
        This allows the AI to update its internal state when making a new guess.
        :param guess: The last guess which has been made.
        :return: Feedback (result) for this guess.
        """
        ...

    @abstractmethod
    def _get_status_of_last_guess_specific(self) -> str:
        """
        This allows the AI to log data more specific to its inner working
        than just the previous guess and the given feedback on it.
        :return: AI specific data to be appended for a certain guess.
        """
        ...
