from math import log2
from typing import Optional

from src.mastermind.ai.ai_exclusion_score_based import AIExclusionScoreBased
from src.mastermind.combination import Combination


class AIExclusionHighestEntropy(AIExclusionScoreBased):
    """
    This AI computes the entropy for each guess based on the current game state and chooses the combination
    with the highest entropy as the next guess.
    """

    def __init__(self, consider_excluded_candidates: bool, first_guess: Optional[Combination] = None):
        # Basically entropy is just the expected value (mean) for the information gain.
        super().__init__(
            score_name="Entropy",
            score_function=lambda probabilities: sum([p * -log2(p) for p in probabilities]),
            consider_excluded_candidates=consider_excluded_candidates,
            first_guess=first_guess if first_guess is not None else Combination.get_random_all_colors_different)
