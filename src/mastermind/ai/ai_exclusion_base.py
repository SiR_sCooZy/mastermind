from abc import abstractmethod
from math import log2

from src.mastermind.ai.ai_base import AIBase
from src.mastermind.combination import Combination
from src.mastermind.game import Game
from src.mastermind.result import Result


class AIExclusionBase(AIBase):
    """
    This class of AI algorithms operate on the exclusion principle.
    This means that they will only make guesses which can theoretically
    still be valid based on all feedback which has been received so far.
    This means that if a certain combination cannot possibly be correct, it will not be chosen as guess.
    """

    def __init__(self):
        super().__init__()
        # All combinations which theoretically can still be the one to be found.
        self._candidate_combinations = None
        # All guesses which have already been made
        self._guessed_combinations = None
        # All guesses which have not been made yet but logically cannot be the right combination (answer).
        self._excluded_combinations = None
        # Information in bits gained by the last guess.
        self._prev_information_gain = 0
        # Total information in bits gained by all guesses so far.
        self._total_information_gain = 0

    def new_game(self, game: Game) -> None:
        super().new_game(game)
        # A new game means all theoretical combinations can be one to be determined.
        self._candidate_combinations = Combination.get_all_combinations()
        self._guessed_combinations = []
        self._excluded_combinations = []
        self._prev_information_gain = 0
        self._total_information_gain = 0

    @property
    def n_remaining_candidates(self) -> int:
        """
        :return: The number of combinations which can theoretically still be
                 the true answer based on the current game state.
        """
        return len(self._candidate_combinations)

    @abstractmethod
    def compute_next_guess(self) -> Combination:
        ...

    def _simulate_next_guess_specific(self, guess: Combination) -> Result:
        true_result = self._game.guess(guess)
        self._guessed_combinations.append(guess)
        updated_combinations = []
        for possible_answer in self._candidate_combinations:
            # Use cache for similarity computation
            guess_result = Combination.get_all_combination_results()[possible_answer.id][guess.id]
            # A combination can only remain a possible candidate for the true combination
            # if it gives the same result (feedback) as the true combination has provided.
            # Do not keep this guess as candidate (even if the game has been won)!
            if possible_answer != guess:
                if guess_result == true_result:
                    updated_combinations.append(possible_answer)
                else:
                    self._excluded_combinations.append(possible_answer)
        # Greetings from Claude Shannon ;).
        self._prev_information_gain = -log2(len(updated_combinations) / len(self._candidate_combinations)) \
            if len(updated_combinations) > 0 else max(0.0, -log2(1 / len(self._candidate_combinations)))
        # Yes, the total information gain is simply the sum of the individually gathered information amounts.
        self._total_information_gain += self._prev_information_gain
        self._candidate_combinations = updated_combinations
        return true_result

    def _get_status_of_last_guess_specific(self) -> str:
        return f"\t-\tInformation Gain: {self._prev_information_gain:.2f}" \
               f"\t-\tRemaining Candidates: {self.n_remaining_candidates}"
