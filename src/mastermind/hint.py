from enum import IntEnum


class Hint(IntEnum):
    """
    A hint is part of the feedback given to the player.
    Each hint gives clues about which placed pins are right and which are wrong.
    Each hint does not refer to a specific position of a combination, but can refer to any position.
    """
    # use prime numbers to be able to easily reconstruct all individual hints from an id for a set of hints
    BLACK = 2  # A pin has the right position and right color
    WHITE = 3  # A pin has the wrong position but right color
    EMPTY = 5  # A pin has the wrong position and wrong color
