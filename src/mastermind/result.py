from __future__ import annotations

from math import prod

from src.mastermind.hint import Hint
from src.util.prime_number_utils import decode_using_prime_factorization


class Result:
    """
    This represents a result (similarity or difference) between two specific combinations.
    A result consists of 4 hints, 1 for each pin.
    There is no positional correspondence between the hints and the pins,
    meaning it is unknown which hint refers to which pin.
    A hint can be used to give players feedback on their guesses
    by computing the similarity to the searched combination.
    """

    def __init__(self, hints: (Hint, Hint, Hint, Hint)):
        self._hints = sorted(hints)
        self._id = prod(self._hints)

    @property
    def id(self) -> int:
        return self._id

    @classmethod
    def decode(cls, result_id: int) -> Result:
        """
        Reconstruct the hint based on its id (prime factor encoding).
        :param result_id: To be decoded.
        :return: The reconstructed result.
        """
        return cls(decode_using_prime_factorization(result_id, Hint))

    @staticmethod
    def calc_possible_results() -> [Result]:
        """
        :return: All possible Results which can exist for the game (order of Hints does not matter).
        """
        hints = [h for h in Hint]
        result_ids = set()
        for i in hints:
            for j in hints:
                for k in hints:
                    for m in hints:
                        s = i * j * k * m
                        result_ids.add(s)
        # impossible to have 1 at incorrect position but all others fully correct
        result_ids.remove(Hint.BLACK ** 3 * Hint.WHITE)
        return sorted([Result.decode(r) for r in result_ids])

    def __str__(self) -> str:
        return "\t".join([str(h).split(".")[1] for h in list(self._hints)])

    def __eq__(self, other: Result) -> bool:
        if other is None:
            return False
        return self.id == other.id

    def __hash__(self) -> int:
        return hash(self.id)

    def __lt__(self, other: Result) -> bool:
        return self.id < other.id
