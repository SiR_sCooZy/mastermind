from typing import Optional

from src.mastermind.combination import Combination
from src.mastermind.result import Result


class Game:
    """
    This class represents a single game of Mastermind and tracks the progress over the entire game.
    This progress consists of all made guesses as well as the given feedback (results) on them.
    """

    def __init__(self, true_combination: Combination):
        self._true_combination = true_combination
        self._guesses = []
        self._results = []

    @property
    def true_combination(self) -> Combination:
        """
        :return: The combination to be determined by the player.
        """
        return self._true_combination

    @property
    def n_guesses(self) -> int:
        """
        :return: The number of guesses the player has made so far.
        """
        return len(self._guesses)

    @property
    def last_guess(self) -> Optional[Combination]:
        """
        :return: The last guess the player has made.
        """
        return self._guesses[-1] if len(self._guesses) > 0 else None

    @property
    def last_result(self) -> Optional[Result]:
        """
        :return: The last feedback (result) the player has received.
        """
        return self._results[-1] if len(self._results) > 0 else None

    def guess(self, guess: Combination) -> Result:
        """
        Make the next guess in the game.
        :param guess: To be made.
        :return: Feedback (result) on the guess, based on the true searched combination.
        """
        self._guesses.append(guess)
        # This computation is done quite rarely, so don't use the cache here.
        # This way if the game would ever be played by a human the entire cache would not have to be setup.
        self._results.append(guess.compute_similarity(self._true_combination))
        return self.last_result

    def is_won(self) -> bool:
        """
        :return: True if the player has won the game by his last guess, False otherwise.
        """
        return self._true_combination == self.last_guess
